extern crate flint;
// extern crate cpuprofiler;
pub mod misc;

use flint::fmpz_poly::FmpzPoly;
use flint::fmpq_poly::FmpqPoly;
use flint::fmpz::Fmpz;
// use flint::fmpq_mat::FmpqMat;
use flint::fmpq::Fmpq;
use misc::prime_sieve;
use flint::arith::{bernoulli_number, ramanujan_tau_series_new};

/// set i th coefficient of `x` to `sigma_{k-1}(i)*l_part`.
fn set_eisen_coeff(k: u64, x: &mut FmpzPoly, l_part: &Fmpz, prec: usize) {
    let expt = k - 1;
    let mut coeffs_vec = vec![Fmpz::from_ui(1); prec];

    let mut mult = Fmpz::new();
    let mut term = Fmpz::new();
    let mut last = Fmpz::new();
    let mut term_m1 = Fmpz::new();
    let mut last_m1 = Fmpz::new();

    for &p in &prime_sieve(prec - 1) {
        let mut p_pow = p as usize;
        mult.set_ui(p as u64);
        mult.set_pow_ui(expt);
        term.mul_mut(&mult, &mult);
        last.set(&mult);

        while p_pow < prec {
            let mut ind = p_pow;
            term_m1.sub_ui_mut(&term, 1);
            last_m1.sub_ui_mut(&last, 1);
            while ind < prec {
                coeffs_vec[ind] *= &term_m1;
                coeffs_vec[ind] /= &last_m1;
                ind += p_pow;
            }
            p_pow *= p as usize;
            last.set(&term);
            term *= &mult;
        }
    }

    for a in coeffs_vec.iter_mut() {
        *a *= l_part;
    }
    for (i, c) in coeffs_vec.iter().enumerate() {
        x.set_coeff(c, i as i64);
    }
    x.set_coeff_si(0, 1);
}

pub fn eisenstein(k: u64, prec: usize) -> FmpqPoly {
    let mut ply = FmpzPoly::new();
    let one: Fmpz = From::from(1);
    set_eisen_coeff(k, &mut ply, &one, prec);
    let mut res = FmpqPoly::new();
    res.set_fmpz_poly(&ply);
    let mut tmp = Fmpq::new();
    let mut brn = Fmpq::new();
    bernoulli_number(&mut brn, k);
    brn.set_inv();
    brn <<= 1;
    brn *= k as i64;
    brn.negate();
    for i in 1..prec {
        res.get_coeff(&mut tmp, i as i64);
        tmp *= &brn;
        res.set_coeff(&tmp, i as i64);
    }
    res
}

pub fn e4_qser(prec: usize) -> FmpzPoly {
    let a: Fmpz = From::from(240);
    let mut x = FmpzPoly::new();
    set_eisen_coeff(4, &mut x, &a, prec);
    x
}

pub fn e6_qser(prec: usize) -> FmpzPoly {
    let a: Fmpz = From::from(-504);
    let mut x = FmpzPoly::new();
    set_eisen_coeff(6, &mut x, &a, prec);
    x
}

fn partitions_4_6(k: usize) -> Vec<(usize, usize)> {
    let mut res = Vec::new();
    for i in 0..(k / 6 + 1) {
        let j = k - 6 * i;
        if j & 0b11 == 0b00 {
            res.push((j >> 2, i));
        }
    }
    res
}

fn tuple_4_6(k: usize) -> Option<(usize, usize)> {
    match k {
        0 => Some((0, 0)),
        2 => None,
        _ => {
            match k % 6 {
                0 => Some((0, k / 6)),
                2 => Some((2, (k - 8) / 6)),
                4 => Some((1, (k - 4) / 6)),
                _ => panic!("This should not happen"),
            }
        }
    }
}

fn eisen_pow(e4: &FmpzPoly, e6: &FmpzPoly, x: &(usize, usize), prec: usize) -> FmpzPoly {
    let (a, b) = *x;
    let prec = prec as i64;
    let mut res = FmpzPoly::new();
    let e4_pow = e4.pow_trunc(a as u64, prec);
    let e6_pow = e6.pow_trunc(b as u64, prec);
    res.mullow_mut(&e4_pow, &e6_pow, prec);
    res
}

pub fn modular_forms_of_weight(k: usize, prec: usize) -> Vec<FmpzPoly> {
    let e4 = e4_qser(prec);
    let e6 = e6_qser(prec);
    partitions_4_6(k)
        .iter()
        .map(|x| eisen_pow(&e4, &e6, x, prec))
        .collect()
}

pub fn modular_forms_echelon_basis(k: usize, prec: usize) -> Vec<FmpzPoly> {
    let mut res = Vec::new();
    let a = tuple_4_6(k).unwrap();
    let e4 = e4_qser(prec);
    let e6 = e6_qser(prec);
    res.push(eisen_pow(&e4, &e6, &a, prec));
    let dl = ramanujan_tau_series_new(prec as i64);
    let mut dl_pow = dl.clone();
    for i in 1..((k / 12) + 1) {
        if let Some(a) = tuple_4_6(k - 12 * i) {
            let f = eisen_pow(&e4, &e6, &a, prec);
            let mut tmp_poly = FmpzPoly::new();
            tmp_poly.mullow_mut(&f, &dl_pow, prec as i64);
            res.push(tmp_poly);
        }
        if i < (k / 12) + 1 {
            dl_pow.mullow_assign(&dl, prec as i64);
        }
    }
    res
}

fn pow_u64(p: u64, n: u64) -> u64 {
    let mut res = 1;
    for _ in 0..n {
        res *= p;
    }
    res
}

/// `V^n(f)`
fn v_op(f: &FmpqPoly, p: u64, n: u64, prec: usize) -> FmpqPoly {
    if n == 0 {
        return f.clone();
    }
    let mut res = FmpqPoly::new();
    let p_pow = pow_u64(p, n) as i64;
    let mut i = 0;
    let mut j = 0;
    let mut tmp = Fmpq::new();
    while i < prec as i64 {
        f.get_coeff(&mut tmp, j);
        res.set_coeff(&tmp, i);
        j += 1;
        i += p_pow;
    }
    res
}

fn modform_mod(f: &FmpqPoly, m: &Fmpz, prec: usize) -> FmpzPoly {
    let mut res = FmpzPoly::new();
    let mut tmpq = Fmpq::new();
    let mut tmp = Fmpz::new();
    for i in 0..prec {
        let i = i as i64;
        f.get_coeff(&mut tmpq, i);
        tmpq.mod_fmpz_mut(&mut tmp, &m).unwrap();
        res.set_coeff(&tmp, i);
    }
    res
}

pub fn eisenstein_diff(k: u64, p: u64, n1: u64, n2: u64, mod_pow: u64, prec: usize) -> FmpzPoly {
    let e = eisenstein(k, prec);
    let p_z: Fmpz = From::from(p as i64);
    let p_pow = p_z.pow(mod_pow);
    let mut f = v_op(&e, p, n1, prec);
    let g = v_op(&e, p, n2, prec);
    f -= &g;
    f /= &p_z;
    modform_mod(&f, &p_pow, prec)
}

pub fn maybe_congruent_to_modforms(
    f: &FmpzPoly,
    k: u64,
    p: u64,
    mod_pow: u64,
    prec: usize,
) -> bool {
    let mut forms = modular_forms_echelon_basis(k as usize, prec);
    let mut tmp = Fmpz::new();
    let mut f = f.clone();
    for (i, b) in forms.iter_mut().enumerate() {
        let i = i as i64;
        f.get_coeff(&mut tmp, i);
        *b *= &tmp as &Fmpz;
        f -= b;
    }
    let p_pow = Into::<Fmpz>::into(p as i64).pow(mod_pow);
    let mut p_pow_mod = Fmpz::new();
    for i in (forms.len() + 1)..prec {
        let i = i as i64;
        f.get_coeff(&mut tmp, i);
        p_pow_mod.mod_mut(&tmp, &p_pow);
        if !p_pow_mod.is_zero() {
            return false;
        }
    }
    true
}

pub fn eisenstein_diff_filt(p: u64, mod_pow: u64, n1: u64, n2: u64, prec: usize) -> u64 {
    let f = eisenstein_diff(p - 1, p, n1, n2, mod_pow, prec);
    let mut l = p - 1;
    let a = pow_u64(p, mod_pow - 1) * (p - 1);
    let mut b = 0;
    loop {
        if maybe_congruent_to_modforms(&f, l, p, mod_pow, prec) {
            return b;
        }
        l += a;
        b += 1;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    // use cpuprofiler::PROFILER;
    use std::time::Instant;


    // Taken from http://qiita.com/pseudo_foxkeh/items/5d5226e3ffa27631e80d
    macro_rules! measure_time {
  ( $x:expr) => {
    {
      let start = Instant::now();
      let result = $x;
      let end = start.elapsed();
      println!("{}.{:03} seconds passed", end.as_secs(), end.subsec_nanos() / 1_000_000);
      result
    }
  };
}


    #[test]
    fn test_set_eisenstein() {
        let e4 = e4_qser(12);
        let mut x = FmpzPoly::new();
        x.mullow_mut(&e4, &e4, 12);
        println!("{}", x);
    }

    #[test]
    fn test_partitions() {
        let v = partitions_4_6(2000);
        assert_eq!(v.len(), 167);
    }

    #[test]
    fn test_modforms() {
        let v = modular_forms_of_weight(12, 10);
        for a in &v {
            println!("{}", a);
        }
    }

    #[test]
    fn test_echelon() {
        let vs = modular_forms_echelon_basis(14, 10);
        for f in &vs {
            println!("{}", f);
        }
    }

    #[test]
    fn test_eisenstein() {
        let e30 = eisenstein(30, 10);
        println!("{}", e30);
    }

    #[test]
    fn test_eisen_diff() {
        let e = eisenstein_diff(4, 5, 1, 0, 2, 100);
        println!("{}", e);
    }

    #[test]
    fn test_maybe_cong_to() {
        let p = 13;
        let prec = 2400;
        let f = eisenstein_diff(p - 1, p, 1, 0, 2, prec);
        for i in 168..169 {
            if maybe_congruent_to_modforms(&f, i, p, 2, prec) {
                println!("{}", i);
                break;
            }
        }
    }

    #[test]
    fn modp2_g_v1_5() {
        let prec = 300;
        // 6
        let k = eisenstein_diff_filt(5, 2, 2, 1, prec);
        println!("{}", k);
    }

    #[test]
    fn modp2_g_v1_7() {
        let prec = 300;
        // 8
        let k = eisenstein_diff_filt(7, 2, 2, 1, prec);
        println!("{}", k);
    }

    #[test]
    fn modp3_g_v0_5() {
        let prec = 300;
        // 1
        let k = eisenstein_diff_filt(5, 3, 1, 0, prec);
        println!("{}", k);
    }

    #[test]
    fn modp3_g_v0_7() {
        let prec = 300;
        // 1
        let k = eisenstein_diff_filt(7, 3, 1, 0, prec);
        println!("{}", k);
    }

    #[test]
    fn modp3_g_v1_5() {
        let prec = 300;
        // 2
        let k = eisenstein_diff_filt(5, 3, 2, 1, prec);
        println!("{}", k);
    }

    #[test]
    fn modp3_g_v1_7() {
        let prec = 300;
        // 2
        let k = eisenstein_diff_filt(7, 3, 2, 1, prec);
        println!("{}", k);
    }

    #[test]
    fn modp3_g_v1_11() {
        let prec = 300;
        // 2
        let k = eisenstein_diff_filt(11, 3, 2, 1, prec);
        println!("{}", k);
    }

    #[test]
    fn modp3_g_v2_5() {
        let prec = 300;
        // 8
        let k = eisenstein_diff_filt(5, 3, 3, 2, prec);
        println!("{}", k);
    }

    #[test]
    fn modp3_g_v2_7() {
        let prec = 500;
        // 10
        // 23.382 seconds passed
        let k = measure_time!(eisenstein_diff_filt(7, 3, 3, 2, prec));
        println!("{}", k);
    }

    #[test]
    fn modp4_g_v2_5() {
        // 1.274 seconds passed
        // 2
        let prec = 500;
        let k = measure_time!(eisenstein_diff_filt(5, 4, 3, 2, prec));
        println!("{}", k);
    }

    #[test]
    fn modp4_g_v2_7() {
        // 12.638 seconds passed
        // 2
        let prec = 500;
        let k = measure_time!(eisenstein_diff_filt(7, 4, 3, 2, prec));
        println!("{}", k);
    }

    #[test]
    fn modp4_g_v3_5() {
        // PROFILER
        //     .lock()
        //     .unwrap()
        //     .start("./my-prof-modp4_g_v3_5.profile")
        //     .unwrap();
        let prec = 700;
        // 9
        let k = eisenstein_diff_filt(5, 4, 4, 3, prec);
        println!("{}", k);
        // PROFILER.lock().unwrap().stop().unwrap();
    }

    #[test]
    fn modp4_g_v3_7() {
        let prec = 2410;
        let k = eisenstein_diff_filt(7, 4, 4, 3, prec);
        println!("{}", k);
    }

    #[test]
    fn modp3_ev2_5() {
        let p = 5;
        let prec = 300;
        let e = eisenstein(p - 1, prec);
        let mod_pow = 3;
        let mut p_pow: Fmpz = From::from(p as i64);
        p_pow.set_pow_ui(mod_pow);
        let ev2 = modform_mod(&v_op(&e, p, 2, prec), &p_pow, prec);
        let expt = pow_u64(p, 2) - 1;
        let f = ev2.pow_trunc(expt, prec as i64);
        for k in 0..1000 {
            let j = (p - 1) * expt + k * pow_u64(p, mod_pow - 1) * (p - 1);
            if maybe_congruent_to_modforms(&f, j, p, 3, prec) {
                println!("{}", k);
                break;
            }
        }
    }
}
